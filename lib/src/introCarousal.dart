import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../utils/Responsive.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Map<String, dynamic>> content = [
    {
      'title': 'Husky',
      'titleFirst': 'Bring a new friend',
      'titleSecond': 'to your home',
      'imagePath': 'lib/images/husky.jpg',
      'descripiton':
          'When you have an apostrophe in a string literal, you need to escape it with a backslash (\) to avoid syntax errors. ',
      'buttonText': 'Get Started'
    },
    {
      'title': 'Parrot',
      'titleFirst': 'Give them the love',
      'titleSecond': 'they deserve',
      'imagePath': 'lib/images/parrot.jpg',
      'descripiton':
          'When you have an apostrophe in a string literal, you need to escape it with a backslash (\) to avoid syntax errors. ',
      'buttonText': 'Continue'
    },
    {
      'title': 'Dog',
      'titleFirst': 'It\'s time to adopt your',
      'titleSecond': 'new best friend',
      'imagePath': 'lib/images/dog.jpg',
      'descripiton':
          'When you have an apostrophe in a string literal, you need to escape it with a backslash (\) to avoid syntax errors. ',
      'buttonText': 'Continue'
    },
  ];
  int _currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _currentIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  // void _nextImage() {
  //   if (_currentIndex == content.length - 1) {
  //     Navigator.pushNamed(context, '/signup');
  //   } else {
  //     setState(() {
  //       _currentIndex = (_currentIndex + 1) % content.length;
  //     });
  //     _pageController.animateToPage(
  //       _currentIndex,
  //       duration: Duration(milliseconds: 500),
  //       curve: Curves.ease,
  //     );
  //   }
  // }

  void _nextImage() {
    if (_currentIndex == content.length - 1) {
      Navigator.pushNamed(context, '/signup');
    } else {
      setState(() {
        _currentIndex = (_currentIndex + 1) % content.length;
      });

      if (_pageController.hasClients) {
        _pageController.animateToPage(
          _currentIndex,
          duration: Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: Responsive.heightToDp(context, 57),
              child: Stack(
                children: [
                  Image.asset(
                    content[_currentIndex]['imagePath'],
                    width: double.maxFinite,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    top: 40.0.h,
                    right: 0.0.w,
                    child: _currentIndex > 0
                        ? TextButton(
                            onPressed: () {
                              Navigator.pushNamed(context, '/signup');
                              // Handle skip button press
                            },
                            child: Text(
                              'Skip',
                              style: TextStyle(color: Colors.pink),
                            ),
                          )
                        : SizedBox.shrink(),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: Responsive.widthToDp(context, 7.h)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    content[_currentIndex]['titleFirst'] ?? '',
                    style: TextStyle(
                      fontSize: Responsive.widthToDp(context, 7.sp),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    content[_currentIndex]['titleSecond'] ?? '',
                    style: TextStyle(
                      fontSize: Responsive.widthToDp(context, 7.sp),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: Responsive.widthToDp(context, 4.h)),
                    padding: EdgeInsets.only(
                        right: Responsive.widthToDp(context, 7.w),
                        left: Responsive.widthToDp(context, 7.w)),
                    child: Text(
                      content[_currentIndex]['descripiton'] ?? '',
                      style: TextStyle(
                        fontSize: Responsive.widthToDp(context, 4.sp),
                        fontWeight: FontWeight.w200,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: Responsive.widthToDp(context, 8.h)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  content.length,
                  (index) => Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    width: _currentIndex == index ? 40.w : 7.w,
                    height: _currentIndex == index ? 5.h : 7.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.w),
                      color: _currentIndex == index
                          ? Colors.pink
                          : Colors.grey[350],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: Responsive.widthToDp(context, 8.h)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: _nextImage,
                    style: ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.pink),
                      foregroundColor: MaterialStatePropertyAll(Colors.white),
                      minimumSize: MaterialStateProperty.all(Size(300.w, 50.h)),
                    ),
                    child: Text(content[_currentIndex]['buttonText'] ?? ''),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
