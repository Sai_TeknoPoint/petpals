import 'package:flutter/widgets.dart';

// class Global {
//   static double fontScale = 1.0; // Set your font scale value
// }

class Responsive {
  static double widthToDp(BuildContext context, double number) {
    double width = MediaQuery.of(context).size.width;
    return (width * number) / 100;
  }

  static double heightToDp(BuildContext context, double number) {
    double height = MediaQuery.of(context).size.height;
    return (height * number) / 100;
  }

  // static double responsiveFont(BuildContext context, double number) {
  //   dynamic fontScale = (MediaQuery.of(context).textScaler);
  //   return fontScale;
  // }

  // static double responsiveFont(BuildContext context, double number) {
  //   return Global.fontScale > 1 ? number / Global.fontScale : number;
  // }
}